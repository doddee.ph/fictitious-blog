package com.fictitious.blog.mapper;

import static org.junit.jupiter.api.Assertions.*;

import com.fictitious.blog.dto.PostDto;
import com.fictitious.blog.entity.Post;
import com.fictitious.blog.entity.Tag;
import java.util.Arrays;
import java.util.HashSet;
import org.junit.jupiter.api.Test;

class PostMapperTest {

  @Test
  void toEntityTest() {
    PostDto postDto = PostDto.builder()
        .title("Bla").content("Lorem ipsum.")
        .tags(new HashSet<>(Arrays.asList("Bali", "Holiday")))
        .build();
    Post actual = PostMapper.MAPPER.toEntity(postDto);
    Post expected = Post.builder()
        .title("Bla").content("Lorem ipsum.")
        .tags(new HashSet<>(Arrays.asList(
            Tag.builder().label("Bali").build(),
            Tag.builder().label("Holiday").build())))
        .build();
    assertEquals(expected.getTitle(), actual.getTitle());
    assertEquals(expected.getContent(), actual.getContent());
    assertEquals(expected.getTags().size(), actual.getTags().size());
  }

  @Test
  void toDtoTest() {
    Post post = Post.builder()
        .title("Bla").content("Lorem ipsum.")
        .tags(new HashSet<>(Arrays.asList(
            Tag.builder().label("Bali").build(),
            Tag.builder().label("Holiday").build())))
        .build();
    PostDto actual = PostMapper.MAPPER.toDto(post);
    PostDto expected = PostDto.builder()
        .title("Bla").content("Lorem ipsum.")
        .tags(new HashSet<>(Arrays.asList("Bali", "Holiday")))
        .build();
    assertEquals(expected.getTitle(), actual.getTitle());
    assertEquals(expected.getContent(), actual.getContent());
    assertEquals(expected.getTags().size(), actual.getTags().size());
    assertTrue(actual.getTags().contains("Bali"));
    assertTrue(actual.getTags().contains("Holiday"));
  }
}