package com.fictitious.blog.mapper;

import static org.junit.jupiter.api.Assertions.*;

import com.fictitious.blog.dto.TagDto;
import com.fictitious.blog.entity.Tag;
import org.junit.jupiter.api.Test;

class TagMapperTest {

  @Test
  void toDtoTest() {
    Tag tag = Tag.builder().label("Holiday").build();
    TagDto actual = TagMapper.MAPPER.toDto(tag);
    TagDto expected = TagDto.builder().label("Holiday").build();
    assertEquals(expected.getLabel(), actual.getLabel());
    assertTrue(actual.getPosts().isEmpty());
  }

  @Test
  void toEntityTest() {
    TagDto tagDto = TagDto.builder().label("Bali").build();
    Tag actual = TagMapper.MAPPER.toEntity(tagDto);
    Tag expected = Tag.builder().label("Bali").build();
    assertEquals(expected.getLabel(), actual.getLabel());
    assertTrue(actual.getPosts().isEmpty());
  }
}