package com.fictitious.blog.dto;

import java.util.HashSet;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PostDto {

  private Long id;

  private String title;

  private String content;

  @Default
  private Set<String> tags = new HashSet<>();
}
