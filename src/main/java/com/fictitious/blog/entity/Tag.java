package com.fictitious.blog.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import java.util.HashSet;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(uniqueConstraints = {
    @UniqueConstraint(columnNames = {"label"})
})
public class Tag {

  @Id
  @GeneratedValue
  private Long id;

  private String label;

  @Default
  @ManyToMany(mappedBy = "tags")
  private Set<Post> posts = new HashSet<>();
}
