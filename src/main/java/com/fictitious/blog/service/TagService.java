package com.fictitious.blog.service;

import com.fictitious.blog.dto.TagDto;
import com.fictitious.blog.entity.Tag;
import com.fictitious.blog.mapper.TagMapper;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response.Status;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@ApplicationScoped
public class TagService {

  @Inject
  EntityManager entityManager;

  @Transactional
  public TagDto create(TagDto tagDto) {
    if (Objects.nonNull(tagDto.getId())) {
      throw new WebApplicationException("Cannot create new Tag with id: " + tagDto.getId() + ".", Status.BAD_REQUEST);
    }
    Tag tag = TagMapper.MAPPER.toEntity(tagDto);
    entityManager.persist(tag);
    return TagMapper.MAPPER.toDto(tag);
  }

  public List<TagDto> findAll() {
    List<Tag> tags = entityManager
        .createQuery("SELECT t FROM Tag t ORDER BY label", Tag.class)
        .getResultList();
    return TagMapper.MAPPER.toDtoList(tags);
  }

  public TagDto findOne(Long id) {
    Tag tag = entityManager.find(Tag.class, id);
    if (Objects.isNull(tag)) {
      throw new WebApplicationException("Find Tag with id: " + id + " does not exist.", Status.NOT_FOUND);
    }
    return TagMapper.MAPPER.toDto(tag);
  }

  @Transactional
  public TagDto update(Long id, TagDto tagDto) {
    Tag tag = entityManager.find(Tag.class, id);
    if (Objects.isNull(tag)) {
      throw new WebApplicationException("Update Tag with id: " + id + " does not exist.", Status.NOT_FOUND);
    }
    tag.setLabel(tagDto.getLabel());
    return TagMapper.MAPPER.toDto(tag);
  }

  @Transactional
  public void delete(Long id) {
    Tag tag = entityManager.find(Tag.class, id);
    if (Objects.isNull(tag)) {
      throw new WebApplicationException("Delete Tag with id: " + id + " does not exist.", Status.NOT_FOUND);
    }
    entityManager.remove(tag);
  }

  public Set<Tag> updateTags(Set<String> labels) {
    Map<String, Tag> existTagMap = entityManager
        .createQuery("SELECT t FROM Tag t WHERE t.label in (?1)", Tag.class)
        .setParameter(1, labels)
        .getResultList()
        .stream()
        .collect(Collectors.toMap(Tag::getLabel, Function.identity()));

    Set<Tag> tags = labels.stream()
        .filter(s -> !existTagMap.containsKey(s))
        .map(s -> {
          Tag tag = Tag.builder().label(s).build();
          entityManager.persist(tag);
          return tag;
        })
        .collect(Collectors.toSet());
    entityManager.flush();

    tags.addAll(existTagMap.values());
    return tags;
  }
}
