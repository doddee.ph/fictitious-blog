package com.fictitious.blog.service;

import com.fictitious.blog.dto.PostDto;
import com.fictitious.blog.entity.Post;
import com.fictitious.blog.entity.Tag;
import com.fictitious.blog.mapper.PostMapper;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response.Status;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@ApplicationScoped
public class PostService {

  @Inject
  TagService tagService;

  @Inject
  EntityManager entityManager;

  @Transactional
  public PostDto create(PostDto postDto) {
    if (Objects.nonNull(postDto.getId())) {
      throw new WebApplicationException("Cannot create new Post with id: " + postDto.getId() + ".", Status.BAD_REQUEST);
    }
    Post post = PostMapper.MAPPER.toEntity(postDto);
    Set<Tag> tags = tagService.updateTags(postDto.getTags());
    post.setTags(tags);
    entityManager.persist(post);
    return PostMapper.MAPPER.toDto(post);
  }

  public List<PostDto> findAll() {
    List<Post> posts = entityManager
        .createQuery("SELECT p FROM Post p ORDER BY title", Post.class)
        .getResultList();
    return PostMapper.MAPPER.toDtoList(posts);
  }

  public PostDto findOne(Long id) {
    Post post = entityManager.find(Post.class, id);
    if (Objects.isNull(post)) {
      throw new WebApplicationException("Find Post with id: " + id + " does not exist.", Status.NOT_FOUND);
    }
    return PostMapper.MAPPER.toDto(post);
  }

  @Transactional
  public PostDto update(Long id, PostDto postDto) {
    Post post = entityManager.find(Post.class, id);
    if (Objects.isNull(post)) {
      throw new WebApplicationException("Update Post with id: " + id + " does not exist.", Status.NOT_FOUND);
    }
    post.setTitle(postDto.getTitle());
    post.setContent(postDto.getContent());
    Set<Tag> updatedTags = tagService.updateTags(postDto.getTags());
    post.setTags(updatedTags);
    return PostMapper.MAPPER.toDto(post);
  }

  @Transactional
  public void delete(Long id) {
    Post post = entityManager.find(Post.class, id);
    if (Objects.isNull(post)) {
      throw new WebApplicationException("Delete Post with id: " + id + " does not exist.", Status.NOT_FOUND);
    }
    entityManager.remove(post);
  }
}
