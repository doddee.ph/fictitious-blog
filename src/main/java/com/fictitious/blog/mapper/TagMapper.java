package com.fictitious.blog.mapper;

import com.fictitious.blog.dto.TagDto;
import com.fictitious.blog.entity.Post;
import com.fictitious.blog.entity.Tag;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class TagMapper {

  public static TagMapper MAPPER = Mappers.getMapper(TagMapper.class);

  public abstract TagDto toDto(Tag tag);

  public abstract Tag toEntity(TagDto tagDto);

  public abstract List<TagDto> toDtoList(List<Tag> tags);

  public Set<Post> buildPosts(Set<String> posts) {
    return Optional.ofNullable(posts)
        .map(coll -> coll.stream().map(s -> Post.builder().title(s).build())
            .collect(Collectors.toSet()))
        .orElse(Collections.emptySet());
  }

  public Set<String> buildPostTitles(Set<Post> posts) {
    return Optional.ofNullable(posts)
        .map(coll -> coll.stream().map(Post::getTitle)
            .collect(Collectors.toSet()))
        .orElse(Collections.emptySet());
  }
}
