package com.fictitious.blog.mapper;

import com.fictitious.blog.dto.PostDto;
import com.fictitious.blog.entity.Post;
import com.fictitious.blog.entity.Tag;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class PostMapper {

  public static PostMapper MAPPER = Mappers.getMapper(PostMapper.class);

  public abstract Post toEntity(PostDto postDto);

  public abstract PostDto toDto(Post post);

  public abstract List<PostDto> toDtoList(List<Post> posts);

  public Set<Tag> buildTags(Set<String> tags) {
    return Optional.ofNullable(tags)
        .map(coll -> coll.stream().map(s -> Tag.builder().label(s).build())
            .collect(Collectors.toSet()))
        .orElse(Collections.emptySet());
  }

  public Set<String> buildTagLabels(Set<Tag> tags) {
    return Optional.ofNullable(tags)
        .map(coll -> coll.stream().map(Tag::getLabel).collect(Collectors.toSet()))
        .orElse(Collections.emptySet());
  }
}
