package com.fictitious.blog.controller;

import com.fictitious.blog.dto.TagDto;
import com.fictitious.blog.service.TagService;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import java.util.List;

@Path("/api/tags")
public class TagController {

  @Inject
  TagService tagService;

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Transactional
  public Response create(TagDto tagDto) {
    TagDto created = tagService.create(tagDto);
    return Response.ok(created).status(Status.CREATED).build();
  }

  @GET
  public Response findAll() {
    List<TagDto> tagDtos = tagService.findAll();
    return Response.ok(tagDtos).status(Status.OK).build();
  }

  @GET
  @Path("/{id}")
  public Response findOne(@PathParam("id") Long id) {
    TagDto tagDto = tagService.findOne(id);
    return Response.ok(tagDto).status(Status.OK).build();
  }

  @PUT
  @Path("/{id}")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public Response update(@PathParam("id") Long id, TagDto tagDto) {
    TagDto updated = tagService.update(id, tagDto);
    return Response.ok(updated).status(Status.OK).build();
  }

  @DELETE
  @Path("/{id}")
  public Response delete(@PathParam("id") Long id) {
    tagService.delete(id);
    return Response.ok().status(Status.NO_CONTENT).build();
  }
}
